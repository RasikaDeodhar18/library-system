package com.training.hackathon.library.rest;

import java.util.List;

import com.training.hackathon.library.model.DVD;
import com.training.hackathon.library.service.DVDService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/books")
public class DVDController {
    private static final Logger LOG = LoggerFactory.getLogger(DVDController.class);

    @Autowired
    DVDService dvdService;

    @RequestMapping(method = RequestMethod.GET)
    public List<DVD> findDVDs() {
        LOG.debug("Get books request received");
        return dvdService.getAllDVDs();
    }

    @RequestMapping(method = RequestMethod.POST)
    public DVD save(@RequestBody DVD dvd) {
        LOG.debug("Post dvd request received");
        return dvdService.save(dvd);
    }

}
