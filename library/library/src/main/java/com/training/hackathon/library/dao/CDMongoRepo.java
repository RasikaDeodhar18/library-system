package com.training.hackathon.library.dao;

import com.training.hackathon.library.model.CD;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface CDMongoRepo extends MongoRepository<CD, String> {

}
