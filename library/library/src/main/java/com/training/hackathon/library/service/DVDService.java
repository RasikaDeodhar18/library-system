package com.training.hackathon.library.service;

import java.util.List;

import com.training.hackathon.library.dao.DVDMongoRepo;
import com.training.hackathon.library.model.DVD;

import org.springframework.beans.factory.annotation.Autowired;

public class DVDService {

    @Autowired
    private DVDMongoRepo dvdMongoRepo;

    public DVD save(DVD book) {
        return dvdMongoRepo.save(book);
    }

    public List<DVD> getAllDVDs() {
        return dvdMongoRepo.findAll();
    }
}
