package com.training.hackathon.library.rest;

import java.util.List;

import com.training.hackathon.library.model.Book;
import com.training.hackathon.library.service.BookService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/books")
public class BookController {
    private static final Logger LOG = LoggerFactory.getLogger(BookController.class);

    @Autowired
    BookService bookService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Book> findBooks() {
        LOG.debug("Get books request received");
        return bookService.getAllBooks();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Book save(@RequestBody Book book) {
        LOG.debug("Post books request received");
        return bookService.save(book);
    }

}
