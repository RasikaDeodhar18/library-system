package com.training.hackathon.library.model;

import org.springframework.beans.factory.annotation.Autowired;

public class Library {
    
    @Autowired
    private Book[] books;

    @Autowired
    private CD[] CDs;

    @Autowired
    private DVD[] DVDs;

    public Book[] getBooks() {
        return books;
    }

    public void setBooks(Book[] books) {
        this.books = books;
    }

    public CD[] getCDs() {
        return CDs;
    }

    public void setCDs(CD[] cDs) {
        CDs = cDs;
    }

    public DVD[] getDVDs() {
        return DVDs;
    }

    public void setDVDs(DVD[] dVDs) {
        DVDs = dVDs;
    }
    
}