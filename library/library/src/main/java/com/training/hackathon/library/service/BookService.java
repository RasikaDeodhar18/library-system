package com.training.hackathon.library.service;

import java.util.List;

import com.training.hackathon.library.dao.BookMongoRepo;
import com.training.hackathon.library.model.Book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BookService {

    @Autowired
    private BookMongoRepo bookMongoRepo;

    public Book save(Book book) {
        return bookMongoRepo.save(book);
    }

    public List<Book> getAllBooks() {
        return bookMongoRepo.findAll();
    }

}
