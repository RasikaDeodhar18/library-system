package com.training.hackathon.library.rest;

import java.util.List;

import com.training.hackathon.library.model.Book;
import com.training.hackathon.library.model.Library;
import com.training.hackathon.library.service.LibraryService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/books")
public class LibraryController {
    private static final Logger LOG = LoggerFactory.getLogger(LibraryController.class);

    @Autowired
    LibraryService libraryService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Library> findAllItems() {
        LOG.debug("Get items request received");
        return libraryService.showAllItems();
    }

    @RequestMapping(method = RequestMethod.PUT)
    public String borrowBook(@RequestBody String bookId) {
        LOG.debug("Get items request received");
        if (libraryService.borrowBook(bookId)) {
            return "Book Issued!";
        }
        return "Book is taken";
    }

}
