package com.training.hackathon.library.dao;

import com.training.hackathon.library.model.Library;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface LibraryMongoRepo extends MongoRepository<Library, String> {

}
