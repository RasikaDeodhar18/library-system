package com.training.hackathon.library.rest;

import java.util.List;

import com.training.hackathon.library.model.CD;
import com.training.hackathon.library.service.CDService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/books")
public class CDController {
    private static final Logger LOG = LoggerFactory.getLogger(CDController.class);

    @Autowired
    CDService cdService;

    @RequestMapping(method = RequestMethod.GET)
    public List<CD> findCDs() {
        LOG.debug("Get books request received");
        return cdService.getAllCDs();
    }

    @RequestMapping(method = RequestMethod.POST)
    public CD save(@RequestBody CD cd) {
        LOG.debug("Post cd request received");
        return cdService.save(cd);
    }

}
