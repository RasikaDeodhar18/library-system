package com.training.hackathon.library.dao;

import com.training.hackathon.library.model.DVD;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface DVDMongoRepo extends MongoRepository<DVD, String> {

}
