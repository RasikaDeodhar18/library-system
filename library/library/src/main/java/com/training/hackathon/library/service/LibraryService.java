package com.training.hackathon.library.service;

import java.util.List;

import com.training.hackathon.library.dao.BookMongoRepo;
import com.training.hackathon.library.dao.CDMongoRepo;
import com.training.hackathon.library.dao.DVDMongoRepo;
import com.training.hackathon.library.dao.LibraryMongoRepo;
import com.training.hackathon.library.model.Book;
import com.training.hackathon.library.model.CD;
import com.training.hackathon.library.model.DVD;
import com.training.hackathon.library.model.Library;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LibraryService {

    @Autowired
    private LibraryMongoRepo libraryMongoRepo;

    @Autowired
    private BookMongoRepo bookMongoRepo;

    @Autowired
    private CDMongoRepo cdMongoRepo;

    @Autowired
    private DVDMongoRepo dvdMongoRepo;

    public List<Library> showAllItems() {
        return libraryMongoRepo.findAll();
    }

    /**
     * @deprecated This method is separately called from Book Service now. But can
     *             be used from here.
     * @return List of Books
     */
    public List<Book> getAllBooks() {
        return bookMongoRepo.findAll();
    }

    public List<CD> getAllCDs() {
        return cdMongoRepo.findAll();
    }

    public List<DVD> getAllDVDs() {
        return dvdMongoRepo.findAll();
    }

    public boolean borrowBook(String bookId) {
        if (bookMongoRepo.findById(bookId).get().isBorrowed())
            return false;

        bookMongoRepo.findById(bookId).get().setBorrowed(true);
        return true;
    }

    public boolean borrowCD(String cdId) {
        if (cdMongoRepo.findById(cdId).get().isBorrowed())
            return false;

        cdMongoRepo.findById(cdId).get().setBorrowed(true);
        return true;
    }

    public boolean borrowDVD(String dvdId) {
        if (dvdMongoRepo.findById(dvdId).get().isBorrowed())
            return false;

        dvdMongoRepo.findById(dvdId).get().setBorrowed(true);
        return true;
    }

    public List<Book> newBooks(List<Book> books) {
        List<Book> response = (List<Book>) bookMongoRepo.saveAll(books);
        return response;
    }

    public List<CD> newCDs(List<CD> cds) {
        List<CD> response = (List<CD>) cdMongoRepo.saveAll(cds);
        return response;
    }

    public List<DVD> newDVDs(List<DVD> dvds) {
        List<DVD> response = (List<DVD>) dvdMongoRepo.saveAll(dvds);
        return response;
    }

}
