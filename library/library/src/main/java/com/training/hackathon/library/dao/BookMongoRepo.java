package com.training.hackathon.library.dao;

import com.training.hackathon.library.model.Book;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface BookMongoRepo extends MongoRepository<Book, String>{
    
}
