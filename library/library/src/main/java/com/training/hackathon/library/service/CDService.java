package com.training.hackathon.library.service;

import java.util.List;

import com.training.hackathon.library.dao.CDMongoRepo;
import com.training.hackathon.library.model.CD;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CDService {

    @Autowired
    private CDMongoRepo cdMongoRepo;

    public CD save(CD book) {
        return cdMongoRepo.save(book);
    }

    public List<CD> getAllCDs() {
        return cdMongoRepo.findAll();
    }
}
