package Project_Library;

public enum ItemType {
    BOOKS("Books"), CDS("CDs"), DVDS("DVDs"), PERIODICALS("Periodicals");

    private String itemType;

    private ItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

}
